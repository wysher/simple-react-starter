var webpack = require('webpack');
var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');

// this packages will be splitted from bundle.js
// because they will not changes frequently and they can be cached by browser
const VENDOR_LIBRARIES = [
    'react',
    'react-dom',
    'prop-types',
];

module.exports = {
    // code splitted files: bundle.js - APP, vendor.js - LIBRARIES
    entry: {
        bundle: './src/index.jsx',
        vendor: VENDOR_LIBRARIES
    },
    output: {
        path: path.join(__dirname, 'dist'),
        // chunkhash property assures that files will have unique name and with that -
        // loads files from disk or download them from server when needed
        filename: '[name].[chunk].js'
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                use: ['babel-loader'],
                exclude: /node_modules/,
            }, {
                test: /\.s?css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        'css-loader?importLoaders=1&modules&localIdentName=[name]__[local]___[hash:base64:5]',
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: function () {
                                    return [
                                        require('precss'),
                                        require('autoprefixer')
                                    ];
                                },
                            },
                        },
                        'sass-loader',
                    ],
                }),
            }, {
                // image loading and optimization
                test: /\.(jpe?g|png|gif|svg)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            // Images smaller than 50 kB will be compiled into base64
                            limit: 50000
                        },
                    },
                    'image-webpack-loader'
                ],
            },
        ],
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
        }),
        new ExtractTextPlugin({
            filename: (getPath) => {
                return getPath('css/[name].css').replace('css/js', 'css');
            },
            allChunks: true,
        }),
        // this plugin prevents duplicating on code splitting it assure that bundle.js
        // file will not contains libraries which are loaded with vendor.js manifest.js
        // file tells browser if vendor.js file is actual or if it needs to be updated
        new webpack.optimize.CommonsChunkPlugin({
            names: ['vendor', 'manifest']
        }),
        // this plugin automatically adds script with src for bundled js files
        new HtmlWebpackPlugin({ template: 'src/index.html' })
    ]
};
