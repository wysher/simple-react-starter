import React from 'react';
import 'sanitize.css/sanitize.css';
import styles from '../../styles/global.scss';

const App = () =>
    <div className={styles.app}>
        Hello World
    </div>
;

export default App;
