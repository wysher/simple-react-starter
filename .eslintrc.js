module.exports = {
    "extends": [
    "airbnb"
],
    "plugins": [
    "react"
],
    "env": {
    "browser": true,
        "jest": true,
        "node": true
},
    "rules": {
    "indent": ["error", 4],
        "no-console": "error",
        "no-confusing-arrow": [0],
        "import/no-extraneous-dependencies": [0],
        "no-underscore-dangle": ["error", {
        "allowAfterThis": true,
        "allowAfterSuper": true
    }],
        "no-unused-expressions": ["error", {
            "allowTernary": true,
        }],
        "comma-dangle": ["error", {
        "functions": "never",
        "arrays": "always-multiline",
        "objects": "always-multiline",
        "imports": "always-multiline",
        "exports": "always-multiline",
    }],
        "max-len": ["error", 120],
        "max-depth": ["error", 4],
        "max-nested-callbacks": ["error", 4],
        "complexity": ["warn", 4],
        "key-spacing": ["error", {
        "multiLine":  { "mode": "minimum" }
    }],
        "react/jsx-indent": ["error", 4],
        "react/jsx-indent-props": ["error", 4],
        "react/forbid-prop-types": ["warn", {
        "forbid": ["any", "array"]
    }],
        "react/forbid-foreign-prop-types": "error"
},
    "parserOptions": {
    "ecmaFeatures": {
        "jsx": true
    }
}
};
